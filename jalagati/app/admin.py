from django.contrib import admin
from . import models

class SessionAdmin(admin.ModelAdmin):
    prepopulated_fields = {"city_id": ("city",)}
admin.site.register(models.Session, SessionAdmin)