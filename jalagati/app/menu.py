from cms.menu_bases import CMSAttachMenu
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from zinnia.models import Category
from menus.base import Modifier


class ListCategoryMenu(CMSAttachMenu):
    """Menu for the categories"""
    name = _('Category Menu')

    def get_nodes(self, request):
        """Return menu's node for categories"""
        nodes = []
        for category in Category.objects.filter(parent=None):
            nodes.append(NavigationNode(title=category.title,
                                        url=category.get_absolute_url(),
                                        id="blog_cat_%d" % category.pk))
        return nodes
        
menu_pool.register_menu(ListCategoryMenu)

class ActiveBlog(Modifier):
    def  modify(self, request, nodes, namespace, root_id, post_cut, breadcrumb):
        if post_cut or breadcrumb:
            return nodes

        if request.get_full_path().startswith('/blog'):
            for node in nodes:
                try:
                    if not node.parent and node.get_attribute('reverse_id') == 'blog_base':
                        node.selected = True
                except KeyError:
                    pass

        return nodes

menu_pool.register_modifier(ActiveBlog)