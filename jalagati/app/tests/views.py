from django.test import TestCase
from django.core.urlresolvers import reverse

class GetListView(TestCase):

    def test_get_call(self):
        rsp = self.client.get(reverse('yoga_sessions'))
        self.assertEqual(rsp.status_code, 200)

    def test_post_call(self):
        rsp = self.client.post(reverse('yoga_sessions'))
        self.assertEqual(rsp.status_code, 200)
       