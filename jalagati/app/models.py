# -*- coding: UTF-8 -*-
from django.db import models
from cms.models import CMSPlugin, Page

class SubMenuPlugin(CMSPlugin):
	root_page = models.ForeignKey(Page)
	start_level = models.SmallIntegerField(default=0)
	end_level = models.SmallIntegerField(default=100)
	extra_inactive = models.SmallIntegerField(default=100)
	extra_active = models.SmallIntegerField(default=100)
	template = models.CharField(max_length=255, default='menu/menu.html')

	def __unicode__(self):
		return unicode(self.root_page)

	def copy_relations(self, oldinstance):
		self.root_page = oldinstance.root_page
		self.save()

class Session(models.Model):
	city = models.CharField(max_length=50, help_text="Ez jelenik meg a városválasztónál")
	city_id = models.SlugField(help_text="Ez egy gép számára adott egyedi azonosító")
	address = models.CharField(max_length=255, blank=True, default='', help_text="Az adott helyszín pontos címe")
	monday = models.TextField(blank=True, verbose_name="Hétfői órák")
	monday_extra = models.TextField(blank=True, verbose_name="Hétfői extra adatok")
	tuesday = models.TextField(blank=True, verbose_name="Keddi órák")
	tuesday_extra = models.TextField(blank=True, verbose_name="Keddi extra adatok")
	wednesday = models.TextField(blank=True, verbose_name="Szerdai órák")
	wednesday_extra = models.TextField(blank=True, verbose_name="Szerdai extra adatok")
	thursday = models.TextField(blank=True, verbose_name="Csütörtöki órák")
	thursday_extra = models.TextField(blank=True, verbose_name="Csütörtöki extra adatok")
	friday = models.TextField(blank=True, verbose_name="Pénteki órák")
	friday_extra = models.TextField(blank=True, verbose_name="Pénteki extra adatok")
	saturday = models.TextField(blank=True, verbose_name="Szombati órák")
	saturday_extra = models.TextField(blank=True, verbose_name="Szombati extra adatok")
	sunday = models.TextField(blank=True, verbose_name="Vasárnapi órák")
	sunday_extra = models.TextField(blank=True, verbose_name="Vasárnapi extra adatok")

	def __unicode__(self):
		return self.city

class SignupFormPlugin(CMSPlugin):
	list_id = models.CharField(max_length=30)

	def __unicode__(self):
		return self.list_id
