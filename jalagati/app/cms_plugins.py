from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models import CMSPlugin
from .models import SubMenuPlugin as SubMenuPluginModel
from .models import Session, SignupFormPlugin
from django.utils.translation import ugettext as _

class SubMenuPlugin(CMSPluginBase):
    model = SubMenuPluginModel # Model where data about this plugin is saved
    name = _("Submenu Plugin") # Name of the plugin
    render_template = "app/submenu_plugin.html" # template to render the plugin with

    def render(self, context, instance, placeholder):
        context.update({'instance':instance})
        return context
plugin_pool.register_plugin(SubMenuPlugin) # register the plugin

class SessionPlugin(CMSPluginBase):
	model = CMSPlugin
	name = _('Yoga Sessions')
	render_template = 'app/yoga_session_plugin.html'

	def render(self, context, instance, placeholder):
		context.update({'object_list': Session.objects.all()})
		return context
plugin_pool.register_plugin(SessionPlugin)

from mailchimp.forms import SubscribeForm

class MailchimpPlugin(CMSPluginBase):
	model = SignupFormPlugin
	name = _('Mailchimp Signup Form')
	render_template = 'mailchimp/subscribe.html'

	def render(self, context, instance, placeholder):
		subscribeForm = SubscribeForm(initial={'list_id': instance.list_id})
		return context.update({
			'csrf_token': context['csrf_token'],
			'form': subscribeForm
			})
plugin_pool.register_plugin(MailchimpPlugin)
