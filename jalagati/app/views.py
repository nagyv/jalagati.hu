from django.views.generic import ListView

class GetListView(ListView):
    """A listview that moves post requests to get"""

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)
