# -*- coding: UTF-8 -*-

from django.conf.urls.defaults import *
from django.views.decorators.csrf import csrf_exempt
import models, views

urlpatterns = patterns('',
    url(r'^sessions/$', 
        csrf_exempt(views.GetListView.as_view(
            queryset=models.Session.objects.all()
            )), 
        name='yoga_sessions'),
)
