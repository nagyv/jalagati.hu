# -*- coding: UTF-8 -*- 
from django.contrib.sites.models import Site
from django.conf import settings

def site(request):
    protocol = 'https' if request.is_secure() else 'http'
    site = Site.objects.get_current()
    base_url = '%s://%s' % (protocol, Site.objects.get_current().domain)
    
    description = u'A Jalagati Jóga Egyesületben többéves orvosi- és jógatapasztalat felhasználásával alakítottunk ki egy olyan új mozgásformát, hogy a mai kor emberének a a legalkalmasabb legyen- jógánk ezáltal fejlődött egyedivé. A Jalagati jógát éppen e miatt – jellegéből adódóan – bárki gyakorolhatja, nemtől, kortól és testalkattól függetlenül, hiszen a gyakorlatok az élettani hatásaik figyelembevételével egyedi módon vannak összeállítva.'
    
    return {
        'protocol': protocol,
        'site': base_url, 
        'site_domain': site.domain, 
        'site_name': site.name, 
        'site_description': description,
        'site_logo': base_url + settings.STATIC_URL + 'img/logo.png'}