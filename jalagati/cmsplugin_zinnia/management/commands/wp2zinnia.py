"""WordPress to Zinnia command module"""
import os
import requests
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

from zinnia.management.commands import wp2zinnia
from cms.plugins.text.models import Text

class Command(wp2zinnia.Command):
    def import_entry(self, title, content, item_node):
        """
        Adding the necessary CMSPlugin too
        """
        entry = super(Command, self).import_entry(title, content, item_node)

        # create the CMSPlugin
        plugin = Text(language='hu', plugin_type='TextPlugin',
            position=None, placeholder=entry.content_placeholder, parent=None,
            body=entry.content.split('<!--:-->')[0]) 
        plugin.save()

        #current_id = item_node.find('{%s}post_id' % WP_NS).text
        #parent_id = item_node.find('%s}post_parent' % WP_NS).text

        return entry

    def import_image(self, entry, items, image_id):
        """
        Requests is more reliable to urlopen
        """
        for item in items:
            post_type = item.find('{%s}post_type' % wp2zinnia.WP_NS).text
            if post_type == 'attachment' and \
                   item.find('{%s}post_id' % wp2zinnia.WP_NS).text == image_id:
                title = 'Attachment %s' % item.find('title').text
                self.write_out(' > %s... ' % title)
                image_url = item.find('{%s}attachment_url' % wp2zinnia.WP_NS).text
                img_tmp = NamedTemporaryFile(delete=True)
                img_tmp.write(requests.get(image_url).content)
                img_tmp.flush()
                entry.image.save(os.path.basename(image_url),
                                 File(img_tmp))
                self.write_out(self.style.ITEM('OK\n'))
