from django.conf.urls.defaults import *
from django.contrib import admin
from django.conf import settings
from django.views.generic.base import TemplateView

from cms.sitemaps import CMSSitemap

from adminplus import AdminSitePlus
admin.site = AdminSitePlus()

admin.autodiscover()

urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    url(r'^maillist/', include('mailchimp.urls', namespace='mailchimp')),
	url(r'^comments/', include('django.contrib.comments.urls')),
	url(r'^channel.html$', 'facebook_comments.views.channel'),
    url(r'^yoga/', include('app.urls')),
    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^googlef3660a6051bf1b77.html$', TemplateView.as_view(
        template_name='googlef3660a6051bf1b77.html')),
    #url(r'^', include('jalagati_home.urls')),
    url(r'^filer/', include('filer.server.urls')),
    url(r'^zinnia/', include('zinnia.urls')),
	url(r'^', include('cms.urls')),

)

if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'', include('django.contrib.staticfiles.urls')),
) + urlpatterns