from django.contrib import admin
from . import models

admin.site.register(models.HomepageBox)
admin.site.register(models.HomepageRow)
admin.site.register(models.HomepageQuote)