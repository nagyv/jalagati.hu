from django.db import models

# Create your models here.

class HomepageBox(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    order = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta():
        ordering = ["order"]
        verbose_name_plural = "homepage boxes"

    def __unicode__(self):
        return self.title


class HomepageRow(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    order = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta():
        verbose_name_plural = "homepage rows"

    def __unicode__(self):
        return self.title

class HomepageQuote(models.Model):
    content = models.TextField()
    active = models.BooleanField()

    class Meta():
        verbose_name_plural = "homepage quotes"

    def __unicode__(self):
        return self.content