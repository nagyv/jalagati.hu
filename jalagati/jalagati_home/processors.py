from . import models


def add_homepage_models_to_context(request):
    '''
    This plugin context processor adds the homepage models to context.
    '''
    boxes = models.HomepageBox.objects.all()
    rows = models.HomepageRow.objects.all()
    quotes = models.HomepageQuote.objects.filter(active=True)
    return {'boxes': boxes, 'rows': rows, 'quotes': quotes}