simplecart
========================

Welcome to the documentation for django-simplecart!


Running the Tests
------------------------------------

You can run the tests with via::

    python setup.py test

or::

    python runtests.py
