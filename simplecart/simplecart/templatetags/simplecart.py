from django import template
from classytags.helpers import InclusionTag

register = template.Library()

@register.tag
class Script(InclusionTag):
    name = "simplecart_script"
    template = "simplecart/script_tag.html"

    def get_context(self, context):
        return {
            'LANGUAGE_CODE': context.get('LANGUAGE_CODE'),
            'STATIC_URL': context.get('STATIC_URL'),
            }

@register.tag
class Checkout(InclusionTag):
    name = "simplecart_customerdata"
    template = "simplecart/checkout_script_tag.html"

    def get_context(self, context):
        return {
            'LANGUAGE_CODE': context.get('LANGUAGE_CODE'),
            'STATIC_URL': context.get('STATIC_URL'),
            }