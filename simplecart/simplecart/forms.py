from django import forms
from django.utils.translation import ugettext as _

REGIONS = (
    ('HU', _('Hungary')),
)

class CustomerForm(forms.Form):
    first_name = forms.CharField(max_length=32)
    last_name = forms.CharField(max_length=32)
    address1 = forms.CharField(max_length=100)
    address2 = forms.CharField(max_length=100, required=False)
    city = forms.CharField(max_length=40)
    country = forms.ChoiceField(choices=REGIONS)
    zip = forms.CharField(max_length=32)
