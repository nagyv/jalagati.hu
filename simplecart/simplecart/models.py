from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Item(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def __unicode__(self):
        return self.name
