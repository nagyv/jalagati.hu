# Create your views here.
import json
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from .models import Item
from .forms import CustomerForm

class ListItemsView(ListView):
    model = Item

class BasketView(TemplateView):
    template_name = 'simplecart/basket.html'

class SuccessLandingView(TemplateView):
    template_name = 'simplecart/success.html'

class CancelLandingView(TemplateView):
    template_name = 'simplecart/cancel.html'

class CreateCustomerView(FormView):
    form_class = CustomerForm
    template_name = 'simplecart/customer_form.html'
    
    def get_success_url(self):
        return reverse('simplecart_checkout') + '?checkout=1'

class CheckoutView(TemplateView):
    template_name = 'simplecart/checkout.html'