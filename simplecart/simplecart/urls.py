from django.conf.urls import *
from simplecart.views import ListItemsView, BasketView, \
    SuccessLandingView, CancelLandingView, CreateCustomerView, \
    CheckoutView

urlpatterns = patterns('',
    url(r'^$', ListItemsView.as_view(), name="simplecart_list"),
    url(r'^basket/$', BasketView.as_view(), name="simplecart_list"),
    url(r'^success/$', SuccessLandingView.as_view(), name="simplecart_success"),
    url(r'^cancel/$', CancelLandingView.as_view(), name="simplecart_cancel"),
    url(r'^customerdata/$', CreateCustomerView.as_view(), name="simplecart_create_customer"),
    url(r'^checkout/$', CheckoutView.as_view(), name="simplecart_checkout"),
)