function showModal( data ) {
  $('#modalContainer').html(data);
  $('#modalContainer').attr('title', 'Checkout');
  simpleCart.update()
  var count = 0;
  var form = $('#modalContainer form');
  form.find('#copy_delivery2invoice').toggle(function(){
    form.children.each(function(){
      if(this.name.indexOf('delivery_') === 0) {
        form.find("#id_invoice-" + this.name.substr(9)).val($(this).val());
      }
    })
  });
  simpleCart.each(function(item, idx) {
    form.append('<input type="hidden" value="' + item.get('pk') + '" name="items-' + idx + '-item" />')
    form.append('<input type="hidden" value="' + item.price() + '" name="items-' + idx + '-unit_price" />')
    form.append('<input type="hidden" value="' + item.quantity() + '" name="items-' + idx + '-quantity" />')
    form.append('<input type="hidden" value="27.5" name="items-' + idx + '-vat" />')
    count = idx
  });
  $('#id_items-TOTAL_FORMS').val(++count);
  $('#modalContainer').dialog({
    modal: true,
    buttons: {
      "Do the checkout": function() {
        $(this).dialog("close");
        // submit the form
        form.ajaxSubmit({
          dataType: 'text', 
          success: function(data) {
            try {
              simpleCart.orderId = parseFloat(data);
            } catch(e) {
              showModal(data);
              return false;
            }
            if(form.find('#id_order-payment_method').val() == 'paypal') {
              simpleCart.checkout();
            } else {
              window.location = window.location.protocol + '//' + window.location.host + '/shop/success/';
            }
          }
        });
      },
      Cancel: function() {
        $( this ).dialog( "close" );
      }
    }
  });
}

function onCheckout( data ){
  if ( simpleCart.orderId ) {
    data.orderId = simpleCart.orderId;
    return true;
  } else {
    $.get('/shop/order/', showModal);
    return false;
  }
}
