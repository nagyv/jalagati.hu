from decimal import Decimal

from django.core.files.base import ContentFile
from django.test import TestCase

from ..models import Item, Customer, Order, OrderedItem

class ItemTest(TestCase):

    def test_thumbnail_generation(self):
        item = Item.objects.create(
            name = 'testitem',
            price = 20,
            image = ContentFile('')
        )


class SetUpCustomerTestCase(TestCase):

    def setUp(self):
        super(SetUpCustomerTestCase, self).setUp()
        self.item = Item.objects.create(
            name = 'testitem',
            price = Decimal('20')
        )
        self.customer = Customer.objects.create(
            contact_name = 'testcontact',
            contact_email = 'test@example.com',
            contact_phone = '1234567890',
            delivery_name = 'testcontact',
            delivery_address = 'testaddress',
            delivery_city = 'testcity',
            delivery_zip_code = 'testzip',
            delivery_country = 'testcountry',
            invoice_name = 'testcontact',
            invoice_address = 'testaddress',
            invoice_city = 'testcity',
            invoice_zip_code = 'testzip',
            invoice_country = 'testcountry',
        )

class OrderTest(SetUpCustomerTestCase):

    def test_status_default_draft(self):
        order = Order.objects.create(
            customer=self.customer,
            payment_method=Order.PaymentMethods.collected
            )
        self.assertEqual(order.status, Order.OrderStates.draft)

    def test_net_total(self):
        order = Order.objects.create(
            customer=self.customer,
            payment_method=Order.PaymentMethods.collected
            )
        self.assertEqual(order.net_total, 0)
        ordered = OrderedItem.objects.create(
            order = order,
            item = self.item,
            quantity = 5,
            unit_price = Decimal('20'),
            vat = Decimal('27.5')
        )
        self.assertEqual(order.net_total, 100)

    def test_gros_total(self):
        order = Order.objects.create(
            customer=self.customer,
            payment_method=Order.PaymentMethods.collected
            )
        self.assertEqual(order.gros_total, 0)
        ordered = OrderedItem.objects.create(
            order = order,
            item = self.item,
            quantity = 5,
            unit_price = Decimal('20'),
            vat = Decimal('27.5')
        )
        self.assertEqual(order.gros_total, 127.5)
    

class OrderedItemTest(SetUpCustomerTestCase):

    def setUp(self):
        super(OrderedItemTest, self).setUp()
        order = Order.objects.create(
            customer=self.customer,
            payment_method=Order.PaymentMethods.collected
        )
        self.ordered = OrderedItem.objects.create(
            order = order,
            item = self.item,
            quantity = 5,
            unit_price = Decimal('20'),
            vat = Decimal('27.5')
        )

    def test_net_total(self):
        self.assertEqual(self.ordered.net_total, 100)

    def test_gros_total(self):
        self.assertEqual(self.ordered.gros_total, 127.5)
