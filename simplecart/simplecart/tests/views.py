import json
from django.test import TestCase
from django.test.utils import override_settings
from simplecart.models import Item

class ViewsTest(TestCase):

    def setUp(self):
        Item.objects.create(name='TestItem', price='1.5')

    def test_view_list(self):
        res = self.client.get('/')
        self.assertEqual(res.status_code, 200)
        self.assertTrue('simplecart/item_list.html' in res.template_name)
        self.assertTrue(res.is_rendered)
        self.assertEqual(len(res.context_data['object_list']), 1)

    def test_basket_view(self):
        res = self.client.get('/basket/')
        self.assertEqual(res.status_code, 200)
        self.assertTrue('simplecart/basket.html' in res.template_name)
        self.assertTrue(res.is_rendered)

    def test_success_view(self):
        res = self.client.get('/success/')
        self.assertEqual(res.status_code, 200)
        self.assertTrue('simplecart/success.html' in res.template_name)
        self.assertTrue(res.is_rendered)        

    def test_cancel_view(self):
        res = self.client.get('/cancel/')
        self.assertEqual(res.status_code, 200)
        self.assertTrue('simplecart/cancel.html' in res.template_name)
        self.assertTrue(res.is_rendered)

    def test_create_customer_view(self):
        res = self.client.post('/customerdata/', data={
            'first_name': 'FN',
            'last_name': 'LN',
            'address1': 'a1',
            'address2': 'a2',
            'city': 'city',
            'country': 'HU',
            'zip': '1234',
            }, follow=True) 
        self.assertEqual(res.status_code, 200)
        self.assertTrue('simplecart/checkout.html' in res.template_name)
        self.assertTrue(res.is_rendered)

ViewsTest = override_settings(ROOT_URLCONF='simplecart.urls')(ViewsTest)
